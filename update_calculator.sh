#!/bin/bash

LOCAL_DIR="/data/project/risk-calculator/wmf-risk-calculator"
TARGET_DIR="/data/project/risk-calculator/public_html/site"
REPO_URL="https://gitlab.wikimedia.org/repos/security/wikimedia-risk-calculator.git"

# clone repo if doesn't exist
if [ ! -d "$LOCAL_DIR" ]; then
    echo "cloning repo"
    git clone "$REPO_URL" "$LOCAL_DIR"
else
    echo "repo exists. skipping clone."
fi

cd "$LOCAL_DIR" || { echo "directory $LOCAL_DIR not found"; exit 1; }

# get latest tag and checkout
git fetch --tags
LATEST_TAG=$(git describe --tags $(git rev-list --tags --max-count=1))
git checkout "$LATEST_TAG"

# pull changes
git pull origin "$LATEST_TAG"

# sync site folder using cp :(
mkdir -p "$TARGET_DIR"
echo "copying site folder to target directory, overwriting..."
cp -rf ./site/* "$TARGET_DIR/"

echo "site folder updated to tag: $LATEST_TAG"